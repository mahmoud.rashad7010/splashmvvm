package com.ERP.pos.UI.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.ERP.pos.AppLogger.Logger;
import com.ERP.pos.R;
import com.ERP.pos.UI.ViewModel.Viewmodel;
import com.ERP.pos.Utilities.Enums.SplashStatus;

import static com.ERP.pos.Utilities.Variables.SplashTimeDliay;

public class Splash extends AppCompatActivity {

Viewmodel viewmodel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        viewmodel= ViewModelProviders.of(this).get(Viewmodel.class);


        new Handler().postDelayed
                (new Runnable() {
                    @Override public void run() {

                        viewmodel.CheckUserSession().observe(Splash.this, new Observer<SplashStatus>() {
                            @Override
                            public void onChanged(SplashStatus splashStatus) {

                                switch (splashStatus)
                                {
                                    case INIT_ERROR:
                                        //TODO display error massage
                                        break;
                                    case LOGENDBEFORE:
                                        // go to main screen
                                        viewmodel.OpenMainActivity(Splash.this);

                                        break;
                                    case NOUserSession:
                                        // go to login screen
                                        viewmodel.OpenMainActivity(Splash.this);
                                        break;
                                    case NOTSPECIFY:
                                        // log unknown error
                                        Logger.d("unknown error ","splash");
                                        break;

                                }

                            }
                        });

                    }
                },SplashTimeDliay);


    }
}

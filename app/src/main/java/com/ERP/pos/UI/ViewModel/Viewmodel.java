package com.ERP.pos.UI.ViewModel;


import android.app.Activity;
import android.app.Application;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.ERP.pos.Pojo.User;
import com.ERP.pos.UI.View.MainActivity;
import com.ERP.pos.Utilities.Enums.Language;
import com.ERP.pos.Utilities.Enums.SplashStatus;
import com.ERP.pos.Utilities.SessionManger;

public class Viewmodel extends AndroidViewModel {


    //region Global Variables
    Application application;

    // endregion

    //region Splash
        // region Splash variable
               MutableLiveData<SplashStatus> splashStatus=new MutableLiveData<>();
        //endregion

        //region Splash fn

    /**
     * this fn chick the User preferences in storage data
     * @return MutableLiveData<SplashStatus>
     */
    public  MutableLiveData<SplashStatus> CheckUserSession(){
        //ToDo check user preferences in storage data  as token
        User user = SessionManger.isUserLogedBefore();
        // user didn't login before
        if (user == null)
        {
            splashStatus.setValue(SplashStatus.NOUserSession);
            return splashStatus;
        }
            // put your code here

        //ToDo chick App version

        int currAppVertion = SessionManger.getInstance(application).getAppVersion();
        int lastAppVersion = getLastAppVersion(currAppVertion);
        if (lastAppVersion != currAppVertion){
            SessionManger.getInstance(application).cleanSession();
            splashStatus.setValue(SplashStatus.INIT_ERROR);
            return splashStatus;
        }


        //get lang from session manger
        Language lang = SessionManger.getInstance(application).getAppLanguage();
        //force specific lang
        SessionManger.getInstance(application).setAppLocale(lang.toString());

         //TODO set user in session manger

        SessionManger.getInstance(application).setUser(user);

        splashStatus.setValue(SplashStatus.LOGENDBEFORE);
        return splashStatus ;

    }

    private int getLastAppVersion(int appVersion) {
        //TODO return last app vertion

        return 1;
    }

    //endregion

    //endregion


    public Viewmodel(@NonNull Application application) {
        super(application);
        this.application = application;

    }

    /**
     * Start Main Activety
     *
      */
    public void OpenMainActivity(Activity activity){

        activity.startActivity(new Intent(activity, MainActivity.class));
        activity.finish();
    }





}

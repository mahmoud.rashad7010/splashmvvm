package com.ERP.pos.UI.View;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.ERP.pos.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}

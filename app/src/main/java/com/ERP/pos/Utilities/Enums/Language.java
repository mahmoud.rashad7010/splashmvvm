package com.ERP.pos.Utilities.Enums;

/**
 * Enum of supported language
 */

public enum Language {

    AR(),
    EN();

    @Override
    public String toString() {
        return this.name();
    }

}

package com.ERP.pos.Utilities;


import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.util.DisplayMetrics;

import com.ERP.pos.Pojo.User;
import com.ERP.pos.Utilities.Enums.Language;

import java.util.Locale;

/**
 * SessionManger it to manage local variable environment like user data and language and app versions
 */
public class SessionManger {
    private static final SessionManger ourInstance = new SessionManger();
    private User user;

    private static Context context;

    public static SessionManger getInstance(Context cont) {
        context =cont;
        return ourInstance;
    }

    private SessionManger() {
    }

    public static User isUserLogedBefore() {
        //TODO check session and return user opj if was loged before other wise return null
        return null;
    }

    public int  getAppVersion (){
        //TODO app version from session
        return 1;
    }

    /**
     *
     * @param language
     * @return true if success to insert language other wise return false
     */
    public boolean setAppLanguage (Language language){
        //TODO save language in shard preference
        return  true;
    }
    public Language getAppLanguage (){
        //ToDo lode Language from Shared preference
        return Language.EN;
    }

    /**
     * helper fn to change the app language
     * @param lang
     */
    public void setAppLocale(String lang){

        Resources resources = context.getResources();
        DisplayMetrics dm = resources.getDisplayMetrics();
        Configuration config = resources.getConfiguration();


        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.JELLY_BEAN_MR1){
            config.setLocale(new Locale(lang.toLowerCase()));
        } else {
            config.locale = new Locale(lang.toLowerCase());
        }
        resources.updateConfiguration(config, dm);

    }

    public void cleanSession() {
        //TODO remove user data
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}

package com.ERP.pos.Utilities.Enums;

public enum SplashStatus {
     NOTSPECIFY(1),
     NOUserSession(2),
     LOGENDBEFORE (3),
     INIT_ERROR(4);

     int ststues;

    SplashStatus(int ststues) {
        this.ststues = ststues;
    }

    @Override
    public String toString() {
        return "SplashStatus{" +
                "ststues=" + ststues +
                '}';
    }

    public int getStstues() {
        return ststues;
    }

    public void setStstues(int ststues) {
        this.ststues = ststues;
    }
}
